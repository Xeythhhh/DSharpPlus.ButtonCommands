using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.ButtonCommands.Converters;
using DSharpPlus.Entities;

namespace DSharpPlus.ButtonCommands.TestBot
{
	public class DiscordMessageConverter : IButtonArgumentConverter<DiscordMessage>
	{
		public async Task<Optional<DiscordMessage>> ConvertAsync(string value, ButtonContext ctx)
		{
			ulong[] ids = value.Split(':').Select(x => ulong.TryParse(x, out ulong y) ? y : 0).ToArray();

			DiscordChannel channel = await ctx.Client.GetChannelAsync(ids[0]);
			if (channel == null) return Optional.FromNoValue<DiscordMessage>();

			DiscordMessage result = await channel.GetMessageAsync(ids[1]);
			return result != null ? Optional.FromValue(result) : Optional.FromNoValue<DiscordMessage>();
		}

		public string ConvertToString(DiscordMessage value) => $"{value.Channel.Id}:{value.Id}";
	}
}